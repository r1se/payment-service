package transport

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/r1se/payment-service/endpoint"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"github.com/gorilla/mux"

	types "gitlab.com/r1se/payment-service/types/payment"
)

// Service.CreatePay encoders/decoders.
func EncodeCreatePageRequest(_ context.Context, r *http.Request, request interface{}) error {
	r.URL.Path += "/pay"
	req := request.(endpoint.CreatePayRequest)

	if req.Pay == nil {
		return fmt.Errorf("empty pay object")
	}

	if strings.TrimSpace(req.Pay.FromAccount) == "" {
		return fmt.Errorf("empty from account")
	}
	if strings.TrimSpace(req.Pay.FromAccount) == "" {
		return fmt.Errorf("empty to account")
	}

	if req.Pay.Amount < 0.0 {
		return fmt.Errorf("amount must be greater zero")
	}
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.Pay); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func DecodeCreatePayRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := endpoint.CreatePayRequest{}
	err := json.NewDecoder(r.Body).Decode(&req.Pay)
	if err != nil {
		return nil, err
	}
	if strings.TrimSpace(req.Pay.FromAccount) == "" {
		return nil, fmt.Errorf("empty from account")
	}
	if strings.TrimSpace(req.Pay.ToAccount) == "" {
		return nil, fmt.Errorf("empty to account")
	}
	if req.Pay.Amount <= 0.0 {
		return nil, fmt.Errorf("amount must be greater zero")
	}
	if req.Pay.Currency <= 0 {
		return nil, fmt.Errorf("currency must be greater zero")
	}

	return req, err
}

func EncodeCreatePayResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.CreatePayResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.WriteHeader(http.StatusCreated)
	return nil
}

func DecodeCreatePayResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.CreatePayResponse{Err: decodeError(r)}, nil
	}
	return endpoint.CreatePayResponse{Err: nil}, nil
}

// Service.GetPay encoders/decoders.
func EncodeGetPayRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(endpoint.GetPayRequest)
	r.URL.Path = "/payment/" + url.QueryEscape(req.PayUUID)
	if strings.TrimSpace(req.PayUUID) == "" {
		return fmt.Errorf("empty pay id")
	}
	return nil
}

func DecodeGetPayRequest(_ context.Context, r *http.Request) (interface{}, error) {
	paymentID := mux.Vars(r)["payment_id"]
	if strings.TrimSpace(paymentID) == "" {
		return nil, fmt.Errorf("empty pay id")
	}
	return endpoint.GetPayRequest{PayUUID: paymentID}, nil
}

func EncodeGetPayResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.GetPayResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Pay)
}

func DecodeGetPayResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.GetPayResponse{Err: decodeError(r)}, nil
	}
	res := endpoint.GetPayResponse{Pay: &types.Payment{}}
	err := json.NewDecoder(r.Body).Decode(res.Pay)
	return res, err
}

// Service.GetPaymentsList encoders/decoders.
func EncodeGetPaymentsRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(endpoint.GetPaymentsRequest)
	r.URL.Path += "/account_list"

	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(&types.FetchParams{PageID: int64(req.PageNum), PageSize: int64(req.PageSize)}); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func DecodeGetPaymentsRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var err error
	pageNum := 1
	iPageSize := 10
	pageID, ok := r.URL.Query()["page_id"]
	if ok {
		pageNum, err = strconv.Atoi(pageID[0])
		if pageNum == 1 {
			pageNum = 0
		}
		if err != nil {
			return nil, err
		}
	}

	pageSize, ok := r.URL.Query()["page_size"]
	if ok {
		iPageSize, err = strconv.Atoi(pageSize[0])
		if iPageSize == 0 {
			iPageSize = 10
		}
		if err != nil {
			return nil, err
		}
	}

	return endpoint.GetPaymentsRequest{PageNum: pageNum, PageSize: iPageSize}, nil
}

func EncodeGetPaymentsResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.GetPaymentsResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Payments)
}

func DecodeGetPaymentsResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.GetPaymentsResponse{Err: decodeError(r)}, nil
	}
	res := endpoint.GetPaymentsResponse{Payments: &types.Payments{}}
	err := json.NewDecoder(r.Body).Decode(res.Payments)
	return res, err
}
