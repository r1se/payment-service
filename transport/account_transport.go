package transport

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/r1se/payment-service/endpoint"
	types "gitlab.com/r1se/payment-service/types/payment"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

//Service.CreateAccount encoders/decoders.
func EncodeCreateAccountRequest(_ context.Context, r *http.Request, request interface{}) error {
	r.URL.Path += "/account"
	req := request.(endpoint.CreateAccountRequest)
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.Account); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func DecodeCreateAccountRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := endpoint.CreateAccountRequest{Account: &types.Account{}}
	err := json.NewDecoder(r.Body).Decode(&req.Account)
	return req, err
}

func EncodeCreateAccountResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.CreateAccountResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Account)
}

func DecodeCreateAccountResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.CreateAccountResponse{Err: decodeError(r)}, nil
	}
	res := endpoint.CreateAccountResponse{Account: &types.Account{}}
	err := json.NewDecoder(r.Body).Decode(res.Account)
	return res, err
}

// Service.DeleteNode encoders/decoders.
func EncodeDeleteAccountRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(endpoint.DeleteAccountRequest)
	r.URL.Path += "/account/" + url.QueryEscape(req.AccountName)

	if req.AccountName == "" {
		return fmt.Errorf("AccountName must be exist")
	}
	return nil
}

func DecodeDeleteAccountRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := endpoint.DeleteAccountRequest{}
	req.AccountName = mux.Vars(r)["account_name"]
	if req.AccountName == "" {
		return nil, fmt.Errorf("AccountName must be exist")
	}
	return req, nil
}

func EncodeDeleteAccountResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.DeleteAccountResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func DecodeDeleteAccountResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.DeleteAccountResponse{Err: decodeError(r)}, nil
	}
	res := endpoint.DeleteAccountResponse{Err: nil}
	return res, nil
}

// Service.UpdateAccount encoders/decoders.
func EncodeUpdateAccountRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(endpoint.UpdateAccountRequest)
	r.URL.Path += "/account/" + url.QueryEscape(req.AccountReq.AccountName)

	if req.AccountReq.AccountName == "" {
		return fmt.Errorf("AccountName must be exist")
	}

	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(req.AccountReq.Account); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func DecodeUpdateAccountRequest(_ context.Context, r *http.Request) (interface{}, error) {
	req := endpoint.UpdateAccountRequest{}
	err := json.NewDecoder(r.Body).Decode(&req.AccountReq)
	return req, err
}

func EncodeUpdateAccountResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.UpdateAccountResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Account)
}

func DecodeUpdateAccountResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.UpdateAccountResponse{Err: decodeError(r)}, nil
	}
	var account *types.Account
	res := endpoint.UpdateAccountResponse{Account: account}
	err := json.NewDecoder(r.Body).Decode(res.Account)
	return res, err
}

// Service.GetAccount encoders/decoders.
func EncodeGetAccountRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(endpoint.GetAccountRequest)
	r.URL.Path += "/account/" + url.QueryEscape(req.AccountName)

	if req.AccountName == "" {
		return fmt.Errorf("AccountName must be exist")
	}
	return nil
}

func DecodeGetAccountRequest(_ context.Context, r *http.Request) (interface{}, error) {
	accountName := mux.Vars(r)["account_name"]
	if accountName == "" {
		return nil, fmt.Errorf("AccountName must be exist")
	}

	return endpoint.GetAccountRequest{AccountName: accountName}, nil
}

func EncodeGetAccountResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.GetAccountResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Account)
}

func DecodeGetAccountResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.GetAccountResponse{Err: decodeError(r)}, nil
	}
	res := endpoint.GetAccountResponse{Account: &types.Account{}}
	err := json.NewDecoder(r.Body).Decode(res.Account)
	return res, err
}

// Service.GetAccountList encoders/decoders.
func EncodeGetAccountsRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(endpoint.GetAccountsRequest)
	r.URL.Path += "/account_list"

	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(&types.FetchParams{PageID: int64(req.PageNum), PageSize: int64(req.PageSize)}); err != nil {
		return err
	}
	r.Body = ioutil.NopCloser(&buf)
	return nil
}

func DecodeGetNodesRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var err error
	pageNum := 1
	iPageSize := 10
	pageID, ok := r.URL.Query()["page_id"]
	if ok {
		pageNum, err = strconv.Atoi(pageID[0])
		if pageNum == 1 {
			pageNum = 0
		}
		if err != nil {
			return nil, err
		}
	}

	pageSize, ok := r.URL.Query()["page_size"]
	if ok {
		iPageSize, err = strconv.Atoi(pageSize[0])
		if iPageSize == 0 {
			iPageSize = 10
		}
		if err != nil {
			return nil, err
		}
	}

	return endpoint.GetAccountsRequest{PageNum: pageNum, PageSize: iPageSize}, nil
}

func EncodeGetAccountsResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(endpoint.GetAccountsResponse)
	if res.Err != nil {
		return encodeError(w, res.Err, true)
	}
	w.Header().Set("Content-Type", "application/json")
	return json.NewEncoder(w).Encode(res.Accounts)
}

func DecodeGetAccountsResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode < 200 || r.StatusCode > 299 {
		return endpoint.GetAccountsResponse{Err: decodeError(r)}, nil
	}
	res := endpoint.GetAccountsResponse{Accounts: &types.Accounts{}}
	err := json.NewDecoder(r.Body).Decode(&res.Accounts)
	return res, err
}
