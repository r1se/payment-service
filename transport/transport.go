package transport

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// responseError describes information about the error returned
type responseError struct {
	Error       string `json:"error"`         // id error
	Description string `json:"description"`   // error description
	URI         string `json:"uri,omitempty"` // URL of the error information
}

// encodeError writes a service error to the given http.ResponseWriter.
func encodeError(w http.ResponseWriter, err error, writeMessage bool) error {
	status := http.StatusBadRequest
	message := err.Error()

	w.WriteHeader(status)
	if writeMessage {
		resErr := &responseError{
			Description: message,
			URI:         "",
		}
		w.Header().Set("Content-Type", "application/json")
		return json.NewEncoder(w).Encode(resErr)
	}
	return nil
}

// decodeError reads a service error from the given *http.Response.
func decodeError(r *http.Response) error {
	return fmt.Errorf("%d", r.StatusCode)
}
