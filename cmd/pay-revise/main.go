package main

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/r1se/payment-service/storage/paymentdb"
	types "gitlab.com/r1se/payment-service/types/payment"
	"os"
)

func main() {
	ctx := context.Background()
	err := godotenv.Load()
	if err != nil {
		err = nil
	}
	// create logger
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	dsn := buildConnectionString(logger)
	database, err := sqlx.Open("postgres", dsn)
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed open connection to database", "err", err)
		os.Exit(1)
	}
	err = database.Ping()
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed to ping database", "err", err)
		os.Exit(1)
	}
	storage, err := paymentdb.New(logger, database)
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed to initialize storage", "err", err)
		os.Exit(1)
	}

	badPay, err := storage.GetBadPay(ctx)
	if err != nil {
		_ = level.Error(logger).Log("pay-revise/GetBadPay error: ", err.Error())
		os.Exit(1)
	}

	for _, payment := range badPay.Payments {
		err := storage.CreatePay(ctx, payment.PaymentID, &types.PayRequest{ToAccount: payment.ToAccount,
			FromAccount: payment.FromAccount,
			Amount:      payment.Amount, Currency: payment.Currency}, true)
		if err != nil {
			_ = level.Error(logger).Log("pay-revise/GetBadPay error: ", err.Error())
		}
	}
}

func buildConnectionString(logger log.Logger) string {
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	if user == "" || pass == "" {
		_ = level.Error(logger).Log("msg", "You must include POSTGRES_USER and POSTGRES_PASSWORD environment variables")
		os.Exit(1)
	}
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dbname := os.Getenv("POSTGRES_DB")
	if host == "" || port == "" || dbname == "" {
		_ = level.Error(logger).Log("msg", "You must include POSTGRES_HOST, POSTGRES_PORT, and POSTGRES_DB environment variables")
		os.Exit(1)
	}

	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, pass, host, port, dbname)
}
