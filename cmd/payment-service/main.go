package main

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/uber/jaeger-client-go/config"
	"gitlab.com/r1se/payment-service/httpserver"
	"gitlab.com/r1se/payment-service/storage/paymentdb"
	"golang.org/x/time/rate"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	exitCodeSuccess = 0
	exitCodeFailure = 1
	RateLimitEvery  = time.Second / 500 //500rps
	RateLimitBurst  = 100
	apiVersion      = "/api/v1"
	defaultHTTPPort = "5000"
)

func main() {
	_, cancel := context.WithCancel(context.Background())
	_ = godotenv.Load()

	// create logger
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	//create jaeger
	_ = config.Configuration{
		Disabled: false, // Nop tracer if True
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
			// Адрес рядом стоящего jaeger-agent, который будет репортить спаны
		},
	}

	/*
		tracer, _, _ := jcfg.NewTracer(
			//cfg.Jaeger.ServiceName,
			config.Logger(jaeger.StdLogger),
		)
		defer closer.Close()*/

	dsn := buildConnectionString(logger)
	database, err := sqlx.Open("postgres", dsn)
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed open connection to database", "err", err)
		os.Exit(exitCodeFailure)
	}
	err = database.Ping()
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed to ping database", "err", err)
		os.Exit(exitCodeFailure)
	}
	storage, err := paymentdb.New(logger, database)
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed to initialize storage", "err", err)
		os.Exit(exitCodeFailure)
	}

	httpPort := os.Getenv("HTTP_PORT")
	if httpPort == "" {
		httpPort = defaultHTTPPort
	}

	serverHTTP, err := httpserver.New(&httpserver.Config{
		Logger:      logger,
		Tracing:     nil,
		Port:        httpPort,
		Storage:     storage,
		RateLimiter: rate.NewLimiter(rate.Every(RateLimitEvery), RateLimitBurst),
		APIversion:  apiVersion,
	})
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed to initialize serverHTTP", "err", err)
		os.Exit(exitCodeFailure)
	}
	go func() {
		_ = level.Info(logger).Log("msg", "starting serverHTTP", "port", httpPort)
		if err := serverHTTP.Run(); err != nil {
			_ = level.Error(logger).Log("msg"+
				"", "serverHTTP run failure", "err", err)
			os.Exit(exitCodeFailure)
		}
	}()

	errCh := make(chan error, 1)
	doneCh := make(chan struct{})
	sigCh := make(chan os.Signal, 1)
	signal.Notify(sigCh, syscall.SIGTERM, os.Interrupt)
	defer func() {
		signal.Stop(sigCh)
		cancel()
	}()

	go func() {
		select {
		case sig := <-sigCh:
			_ = level.Info(logger).Log("msg", "received signal, exiting", "signal", sig)
			serverHTTP.Shutdown()
			//			mongoDB.Shutdown()    // Shutdown MongoDB
			signal.Stop(sigCh)
			close(doneCh)
		case <-errCh:
			_ = level.Info(logger).Log("msg", "now exiting with error", "error code", exitCodeFailure)
			os.Exit(exitCodeFailure)
		}
	}()

	<-doneCh
	_ = level.Info(logger).Log("msg", "goodbye")
	os.Exit(exitCodeSuccess)
}

func buildConnectionString(logger log.Logger) string {
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	if user == "" || pass == "" {
		_ = level.Error(logger).Log("msg", "You must include POSTGRES_USER and POSTGRES_PASSWORD environment variables")
		os.Exit(exitCodeFailure)
	}
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dbname := os.Getenv("POSTGRES_DB")
	if host == "" || port == "" || dbname == "" {
		_ = level.Error(logger).Log("msg", "You must include POSTGRES_HOST, POSTGRES_PORT, and POSTGRES_DB environment variables")
		os.Exit(exitCodeFailure)
	}

	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, pass, host, port, dbname)
}
