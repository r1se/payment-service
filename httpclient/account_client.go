package httpclient

import (
	"context"
	"gitlab.com/r1se/payment-service/endpoint"
	types "gitlab.com/r1se/payment-service/types/payment"
)

func (c *Client) CreateAccount(ctx context.Context, account *types.Account) (*types.Account, error) {
	request := endpoint.CreateAccountRequest{Account: account}
	response, err := c.createAccount(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(endpoint.CreateAccountResponse)
	return res.Account, res.Err
}

func (c *Client) DeleteAccount(ctx context.Context, accountName string) error {
	request := endpoint.DeleteAccountRequest{AccountName: accountName}
	response, err := c.deleteAccount(ctx, request)
	if err != nil {
		return err
	}
	res := response.(endpoint.DeleteAccountResponse)
	return res.Err
}

func (c *Client) UpdateAccount(ctx context.Context, accountName string, account *types.Account) (*types.Account, error) {
	request := endpoint.UpdateAccountRequest{AccountReq: &types.AccountRequest{AccountName: accountName, Account: account}}
	response, err := c.updateAccount(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(endpoint.UpdateAccountResponse)
	return res.Account, res.Err
}

func (c *Client) GetAccount(ctx context.Context, accountName string) (*types.Account, error) {
	request := endpoint.GetAccountRequest{AccountName: accountName}
	response, err := c.getAccount(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(endpoint.GetAccountResponse)
	return res.Account, res.Err
}

func (c *Client) GetAccountsList(ctx context.Context, pageNum, pageSize int) (*types.Accounts, error) {
	request := endpoint.GetAccountsRequest{PageNum: pageNum, PageSize: pageSize}
	response, err := c.getAccounts(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(endpoint.GetAccountsResponse)
	return res.Accounts, res.Err
}
