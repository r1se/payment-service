package httpclient

import (
	"context"
	"gitlab.com/r1se/payment-service/endpoint"
	types "gitlab.com/r1se/payment-service/types/payment"
)

func (c *Client) CreatePay(ctx context.Context, payRequest *types.PayRequest) error {
	request := endpoint.CreatePayRequest{Pay: payRequest}
	response, err := c.createPay(ctx, request)
	if err != nil {
		return err
	}
	res := response.(endpoint.CreatePayResponse)
	return res.Err
}

func (c *Client) GetPayment(ctx context.Context, payUUID string) (*types.Payment, error) {
	request := endpoint.GetPayRequest{PayUUID: payUUID}
	response, err := c.getPay(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(endpoint.GetPayResponse)
	return res.Pay, res.Err
}

func (c *Client) GetPaymentsList(ctx context.Context, pageNum, pageSize int) (*types.Payments, error) {
	request := endpoint.GetPaymentsRequest{PageNum: pageNum, PageSize: pageSize}
	response, err := c.getPayments(ctx, request)
	if err != nil {
		return nil, err
	}
	res := response.(endpoint.GetPaymentsResponse)
	return res.Payments, res.Err
}
