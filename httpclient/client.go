package httpclient

import (
	"net/url"

	"github.com/go-kit/kit/endpoint"

	kithttp "github.com/go-kit/kit/transport/http"
	paymentTransport "gitlab.com/r1se/payment-service/transport"
)

// Client is a client for hoop4 service.
type Client struct {
	// Account client
	createAccount endpoint.Endpoint
	deleteAccount endpoint.Endpoint
	getAccount    endpoint.Endpoint
	updateAccount endpoint.Endpoint
	getAccounts   endpoint.Endpoint

	// Pay client
	createPay   endpoint.Endpoint
	getPay      endpoint.Endpoint
	getPayments endpoint.Endpoint
}

// NewClient creates a new hoop4 service client.
func NewClient(serviceURL string) (*Client, error) {
	baseURL, err := url.Parse(serviceURL)
	if err != nil {
		return nil, err
	}

	c := &Client{

		// Account client
		createAccount: kithttp.NewClient(
			"POST",
			baseURL,
			paymentTransport.EncodeCreateAccountRequest,
			paymentTransport.DecodeCreateAccountResponse,
		).Endpoint(),

		deleteAccount: kithttp.NewClient(
			"DELETE",
			baseURL,
			paymentTransport.EncodeDeleteAccountRequest,
			paymentTransport.DecodeDeleteAccountResponse,
		).Endpoint(),

		getAccount: kithttp.NewClient(
			"GET",
			baseURL,
			paymentTransport.EncodeGetAccountRequest,
			paymentTransport.DecodeGetAccountResponse,
		).Endpoint(),

		updateAccount: kithttp.NewClient(
			"PUT",
			baseURL,
			paymentTransport.EncodeUpdateAccountRequest,
			paymentTransport.DecodeUpdateAccountResponse,
		).Endpoint(),

		getAccounts: kithttp.NewClient(
			"POST",
			baseURL,
			paymentTransport.EncodeGetAccountsRequest,
			paymentTransport.DecodeGetAccountsResponse,
		).Endpoint(),

		// Pay client
		createPay: kithttp.NewClient(
			"POST",
			baseURL,
			paymentTransport.EncodeCreatePageRequest,
			paymentTransport.DecodeCreatePayResponse,
		).Endpoint(),

		getPay: kithttp.NewClient(
			"GET",
			baseURL,
			paymentTransport.EncodeGetPayRequest,
			paymentTransport.DecodeGetPayResponse,
		).Endpoint(),

		getPayments: kithttp.NewClient(
			"POST",
			baseURL,
			paymentTransport.EncodeGetPaymentsRequest,
			paymentTransport.DecodeGetPaymentsResponse,
		).Endpoint(),
	}

	return c, nil
}
