module gitlab.com/r1se/payment-service

go 1.14

require (
	github.com/go-kit/kit v0.10.0
	github.com/go-openapi/errors v0.19.7
	github.com/go-openapi/strfmt v0.19.5
	github.com/go-openapi/swag v0.19.9
	github.com/go-openapi/validate v0.19.11
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gojuno/minimock/v3 v3.0.8
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.3
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.8.0
	github.com/mattn/go-sqlite3 v1.14.3 // indirect
	github.com/opentracing/opentracing-go v1.1.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pressly/goose v2.6.0+incompatible
	github.com/prometheus/client_golang v1.7.1
	github.com/shopspring/decimal v1.2.0
	github.com/stretchr/testify v1.6.1
	github.com/uber/jaeger-client-go v2.25.0+incompatible
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
)
