package middleware

import (
	"context"
	"github.com/go-kit/kit/metrics"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"gitlab.com/r1se/payment-service/service"
	types "gitlab.com/r1se/payment-service/types/payment"
)

// LoggingMiddleware wraps Service and logs request information to the provided Logger.
type LoggingMiddleware struct {
	Service        service.Service
	Logger         log.Logger
	RequestCount   metrics.Counter
	RequestLatency metrics.Histogram
}

func (m *LoggingMiddleware) CreateAccount(ctx context.Context, account *types.Account) (*types.Account, error) {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "CreateAccount"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	account, err := m.Service.CreateAccount(ctx, account)
	_ = level.Info(m.Logger).Log(
		"method", "CreateAccount",
		"err", err,
		"elapsed", time.Since(begin),
	)

	return account, err
}

func (m *LoggingMiddleware) DeleteAccount(ctx context.Context, accountName string) error {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "DeleteAccount"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	err := m.Service.DeleteAccount(ctx, accountName)
	_ = level.Info(m.Logger).Log(
		"method", "DeleteAccount",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return err
}

func (m *LoggingMiddleware) UpdateAccount(ctx context.Context, accountName string, account *types.Account) (*types.Account, error) {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "UpdateAccount"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	updAccount, err := m.Service.UpdateAccount(ctx, accountName, account)
	_ = level.Info(m.Logger).Log(
		"method", "UpdateAccount",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return updAccount, err
}

func (m *LoggingMiddleware) GetAccount(ctx context.Context, accountName string) (*types.Account, error) {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "GetAccount"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	account, err := m.Service.GetAccount(ctx, accountName)
	_ = level.Info(m.Logger).Log(
		"method", "GetAccount",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return account, err
}

func (m *LoggingMiddleware) GetAccountsList(ctx context.Context, pageNum, pageSize int) (*types.Accounts, error) {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "GetAccountsList"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	accounts, err := m.Service.GetAccountsList(ctx, pageNum, pageSize)
	_ = level.Info(m.Logger).Log(
		"method", "GetAccountsList",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return accounts, err
}
