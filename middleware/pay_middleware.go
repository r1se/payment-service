package middleware

import (
	"context"
	"time"

	"github.com/go-kit/kit/log/level"

	types "gitlab.com/r1se/payment-service/types/payment"
)

func (m *LoggingMiddleware) GetPaymentsList(ctx context.Context, pageNum, pageSize int) (*types.Payments, error) {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "GetPaymentsList"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	payments, err := m.Service.GetPaymentsList(ctx, pageNum, pageSize)
	_ = level.Info(m.Logger).Log(
		"method", "GetPaymentsList",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return payments, err
}
func (m *LoggingMiddleware) GetPayment(ctx context.Context, payUUID string) (*types.Payment, error) {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "GetPayment"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	payment, err := m.Service.GetPayment(ctx, payUUID)
	_ = level.Info(m.Logger).Log(
		"method", "GetPayment",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return payment, err
}

func (m *LoggingMiddleware) CreatePay(ctx context.Context, payRequest *types.PayRequest) error {
	begin := time.Now()
	defer func(begin time.Time) {
		lvs := []string{"method", "CreatePay"}
		m.RequestCount.With(lvs...).Add(1)
		m.RequestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(begin)

	err := m.Service.CreatePay(ctx, payRequest)
	_ = level.Info(m.Logger).Log(
		"method", "CreatePay",
		"err", err,
		"elapsed", time.Since(begin),
	)
	return err
}
