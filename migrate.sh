#!/bin/sh

export MIGRATION_DIR=./migrations

if [ "$1" = "--localhost" ]; then
    export POSTGRES_HOST="127.0.0.1"
    export POSTGRES_PORT="54320"
    export POSTGRES_DB="payment"
    export POSTGRES_USER="postgres"
    export POSTGRES_PASSWORD="postgres"
fi

goose -dir ${MIGRATION_DIR} postgres "host=${POSTGRES_HOST} port=${POSTGRES_PORT} dbname=${POSTGRES_DB} user=${POSTGRES_USER} password=${POSTGRES_PASSWORD} sslmode=disable" up
