-- +goose Up

SET timezone = 'UTC';

CREATE TABLE "accounts" (
                            "id" serial NOT NULL UNIQUE,
                            "account" TEXT NOT NULL UNIQUE,
                            "currency" integer NOT NULL DEFAULT 1,
                            "amount" decimal NOT NULL,
                            "active" BOOLEAN NOT NULL DEFAULT TRUE,
                            "deleted" BOOLEAN NOT NULL DEFAULT FALSE,
                            CONSTRAINT "accounts_pk" PRIMARY KEY ("id")
);



CREATE TABLE "currency_type" (
                                 "id" serial NOT NULL,
                                 "sys_name" varchar(4) NOT NULL UNIQUE,
                                 "description" TEXT,
                                 CONSTRAINT "currency_type_pk" PRIMARY KEY ("id")
);

CREATE TABLE "operations_type" (
                                   "id" serial NOT NULL,
                                   "sys_name" TEXT NOT NULL UNIQUE,
                                   "description" TEXT,
                                   CONSTRAINT "operations_type_pk" PRIMARY KEY ("id")
);


CREATE TABLE "pay_operations" (
                                  "transaction_id" uuid NOT NULL,
                                  "account_from" TEXT NOT NULL,
                                  "account_to" TEXT NOT NULL,
                                  "operation" int NOT NULL,
                                  "amount" decimal NOT NULL,
                                  "currency" int NOT NULL,
                                  "is_ended" bool NOT NULL DEFAULT FALSE,
                                  "created_at" TIMESTAMP NOT NULL DEFAULT NOW(),
                                  "updated_at" TIMESTAMP,
                                  "error_info" TEXT NOT NULL DEFAULT '',
                                  CONSTRAINT "pay_operations_pk" UNIQUE ("transaction_id", "operation", "is_ended")
);

ALTER TABLE "accounts" ADD CONSTRAINT "accounts_fk0" FOREIGN KEY ("currency") REFERENCES "currency_type"("id");
ALTER TABLE "pay_operations" ADD CONSTRAINT "pay_operations_fk0" FOREIGN KEY ("account_from") REFERENCES "accounts"("account") ON DELETE CASCADE;
ALTER TABLE "pay_operations" ADD CONSTRAINT "pay_operations_fk1" FOREIGN KEY ("account_to") REFERENCES "accounts"("account") ON DELETE CASCADE;
ALTER TABLE "pay_operations" ADD CONSTRAINT "pay_operations_fk2" FOREIGN KEY ("operation") REFERENCES "operations_type"("id");
ALTER TABLE "pay_operations" ADD CONSTRAINT "pay_operations_fk3" FOREIGN KEY ("currency") REFERENCES "currency_type"("id");


INSERT INTO currency_type  (sys_name, description) VALUES ('USD', 'Американский доллар'), ('RUB', 'Российский рубль'), ('THB', 'Тайский бат');

INSERT INTO operations_type (sys_name, description) VALUES ('outgoing', 'Исходящий платеж'), ('incoming', 'Входящий платеж'), ('blocked', 'Заблокированный платеж');

-- +goose Down


ALTER TABLE "accounts" DROP CONSTRAINT IF EXISTS "accounts_fk0";

ALTER TABLE "pay_operations" DROP CONSTRAINT IF EXISTS "pay_operations_fk0";

ALTER TABLE "pay_operations" DROP CONSTRAINT IF EXISTS "pay_operations_fk1";

ALTER TABLE "pay_operations" DROP CONSTRAINT IF EXISTS "pay_operations_fk2";

ALTER TABLE "pay_operations" DROP CONSTRAINT IF EXISTS "pay_operations_fk3";

DROP TABLE IF EXISTS "accounts";

DROP TABLE IF EXISTS "currency_type";

DROP TABLE IF EXISTS "operations_type";

DROP TABLE IF EXISTS "pay_operations";

