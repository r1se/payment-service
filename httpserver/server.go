package httpserver

import (
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/r1se/payment-service/service"
	"gitlab.com/r1se/payment-service/storage"
	"golang.org/x/time/rate"
	"net/http"
	"time"
)

// ServerHTTP is a payment service http server.
type ServerHTTP struct {
	logger log.Logger
	srv    *http.Server
}

// Config is a http server configuration.
type Config struct {
	Logger      log.Logger
	Tracing     opentracing.Tracer
	Port        string
	Storage     storage.Storage
	RateLimiter *rate.Limiter
	APIversion  string
}

// New creates a new http server.
func New(cfg *Config) (*ServerHTTP, error) {
	mux := http.NewServeMux()

	srv := &http.Server{
		Addr:         ":" + cfg.Port,
		Handler:      mux,
		ReadTimeout:  40 * time.Second,
		WriteTimeout: 40 * time.Second,
	}

	server := &ServerHTTP{
		logger: cfg.Logger,
		srv:    srv,
	}

	svc := service.NewService(cfg.Storage, cfg.Logger, cfg.Tracing)

	handler := newHandlers(&handlerConfig{
		svc:         svc,
		tracer:      cfg.Tracing,
		logger:      cfg.Logger,
		rateLimiter: cfg.RateLimiter,
		apiVersion:  cfg.APIversion,
	})

	mux.Handle("/", accessControl(handler))

	return server, nil
}

// CORS headers
func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

// Run starts the server.
func (s *ServerHTTP) Run() error {
	err := s.srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		return err
	}
	return nil
}

// Shutdown stopped the http server.
func (s *ServerHTTP) Shutdown() {
	err := s.srv.Close()
	if err != nil {
		err := level.Info(s.logger).Log("msg", "httpserver: shutdown has err", "err:", err)
		if err != nil {
			return
		}
	}
	err = level.Info(s.logger).Log("msg", "httpserver: shutdown complete")
	if err != nil {
		return
	}
}
