package httpserver

//
//import (
//	"context"
//	"net/http/httptest"
//	"testing"
//
//	"gitlab.com/r1se/payment-service/httpclient"
//
//	"golang.org/x/time/rate"
//
//	"github.com/go-kit/kit/log"
//
//	types "gitlab.com/r1se/payment-service/types/payment"
//)
//
//type mockService struct {
//	// Pay service
//	onCreatePage                    func(ctx context.Context, page *types.Pay) error
//	onCreatePageContent             func(ctx context.Context, pageID string, contents *types.Content) error
//	onGetPage                       func(ctx context.Context, pageID string) (*types.Pay, error)
//	onGetPageWithContentCurrent     func(ctx context.Context, pageID string) (*types.Pay, error)
//	onGetPageWithContentID          func(ctx context.Context, pageID, contentID string) (*types.Pay, error)
//	onGetPageWithContentCreatedDate func(ctx context.Context, pageID string, CreatedDate string) (*types.Pay, error)
//
//	// Account service
//	onCreateNode       func(ctx context.Context, node *types.Account) (*types.Account, error)
//	onDeleteNode       func(ctx context.Context, nodeID types.UUID, force bool) error
//	onGetNodeByID      func(ctx context.Context, nodeID types.UUID, version string) (*types.Account, error)
//	onUpdateNode       func(ctx context.Context, nodeID types.UUID, node *types.Account) (*types.Account, error)
//	onGetNodesByParent func(ctx context.Context, nodeID types.UUID, version string) ([]*types.Account, error)
//
//	//Site service
//	onCreateSite  func(ctx context.Context, site *types.Site) (*types.Site, error)
//	onUpdateSite  func(ctx context.Context, siteID string, site *types.Site) (*types.Site, error)
//	onDeleteSite  func(ctx context.Context, siteID string) error
//	onGetSite     func(ctx context.Context, siteID string) (*types.Site, error)
//	onGetSiteList func(ctx context.Context) ([]*types.Site, error)
//}
//
//// Pay service
//func (s *mockService) CreateAccount(ctx context.Context, page *types.Pay) error {
//	return s.onCreatePage(ctx, page)
//}
//func (s *mockService) CreatePageContent(ctx context.Context, pageID string, contents *types.Content) error {
//	return s.onCreatePageContent(ctx, pageID, contents)
//}
//func (s *mockService) GetAccount(ctx context.Context, pageID string) (*types.Pay, error) {
//	return s.onGetPage(ctx, pageID)
//}
//func (s *mockService) GetPageWithContentCurrent(ctx context.Context, pageID string) (*types.Pay, error) {
//	return s.onGetPageWithContentCurrent(ctx, pageID)
//}
//func (s *mockService) GetPageWithContentID(ctx context.Context, pageID, contentID string) (*types.Pay, error) {
//	return s.onGetPageWithContentID(ctx, pageID, contentID)
//}
//func (s *mockService) GetPageWithContentCreatedDate(ctx context.Context, pageID string, CreatedDate string) (*types.Pay, error) {
//	return s.onGetPageWithContentCreatedDate(ctx, pageID, CreatedDate)
//}
//
//// Account service
//func (s *mockService) CreateNode(node *types.Account) (*types.Account, error) {
//	return s.onCreateNode(ctx, node)
//}
//func (s *mockService) DeleteNode(ctx context.Context, nodeID types.UUID, force bool) error {
//	return s.onDeleteNode(ctx, nodeID, force)
//}
//
//func (s *mockService) GetNode(ctx context.Context, nodeID types.UUID, version string) (*types.Account, error) {
//	return s.onGetNodeByID(ctx, nodeID, version)
//}
//
//func (s *mockService) UpdateNode(ctx context.Context, nodeID types.UUID, node *types.Account) (*types.Account, error) {
//	return s.onUpdateNode(ctx, nodeID, node)
//}
//
//func (s *mockService) GetNodesByParent(ctx context.Context, nodeID types.UUID, version string) ([]*types.Account, error) {
//	return s.onGetNodesByParent(ctx, nodeID, version)
//}
//
////Site service
//func (s *mockService) CreateSite(ctx context.Context, site *types.Site) (*types.Site, error) {
//	return s.onCreateSite(ctx, site)
//}
//func (s *mockService) UpdateSite(ctx context.Context, siteID string, site *types.Site) (*types.Site, error) {
//	return s.onUpdateSite(ctx, siteID, site)
//}
//func (s *mockService) DeleteSite(ctx context.Context, siteID string) error {
//	return s.onDeleteSite(ctx, siteID)
//}
//func (s *mockService) GetSite(ctx context.Context, siteID string) (*types.Site, error) {
//	return s.onGetSite(ctx, siteID)
//}
//func (s *mockService) GetSiteList(ctx context.Context) ([]*types.Site, error) {
//	return s.onGetSiteList(ctx)
//}
//
//func startTestServer(t *testing.T) (*httptest.Server, *httpclient.Client, *mockService) {
//	svc := &mockService{}
//
//	handler := newHandlers(&handlerConfig{
//		svc:         svc,
//		logger:      log.NewNopLogger(),
//		rateLimiter: rate.NewLimiter(rate.Inf, 1),
//		apiVersion:  "/api/v1",
//	})
//
//	server := httptest.NewServer(handler)
//
//	server.URL += "/api/v1"
//
//	client, err := httpclient.NewClient(server.URL)
//	if err != nil {
//		t.Fatal(err)
//	}
//
//	return server, client, svc
//}
//
///*
//func TestCreatePage(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name string
//		page *types.Pay
//		err  error
//	}{
//		{
//			name: "ok response",
//			page: &types.Pay{
//				ID:       "1",
//				Own:      "site",
//				Name:     "index.md",
//				Contents: nil,
//			},
//			err: nil,
//		},
//		{
//			name: "error response",
//			page: nil,
//			err:  &types.Error{Kind: types.ErrBadParams, Message: "empty page"},
//		},
//		{
//			name: "error response",
//			page: &types.Pay{
//				ID:       "1",
//				Own:      "site",
//				Name:     "",
//				Contents: nil,
//			},
//			err: &types.Error{Kind: types.ErrBadParams, Message: "empty page name"},
//		},
//		{
//			name: "error response",
//			page: &types.Pay{
//				ID:       "1",
//				Own:      "",
//				Name:     "index.md",
//				Contents: nil,
//			},
//			err: &types.Error{Kind: types.ErrBadParams, Message: "empty page owner"},
//		},
//		{
//			name: "error response",
//			page: &types.Pay{
//				ID:   "1",
//				Own:  "site",
//				Name: "index.md",
//				Contents: []*types.Content{
//					{
//						ID:          "1",
//						Current:     true,
//						CreatedDate: 0,
//						Content:     "World!",
//					},
//					{
//						ID:          "2",
//						Current:     true,
//						CreatedDate: 0,
//						Content:     "Hello!",
//					},
//				},
//			},
//			err: &types.Error{Kind: types.ErrBadParams, Message: "current content can be only one"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onCreatePage = func(ctx context.Context, page *types.Pay) error {
//				return tc.err
//			}
//			gotErr := client.CreateAccount(context.Background(), tc.page)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//
//		})
//	}
//}
//
//func TestCreatePageContent(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name     string
//		pageID   string
//		contents *types.Content
//		err      error
//	}{
//		{
//			name:   "ok response",
//			pageID: "1",
//			contents: &types.Content{
//				ID:          "1",
//				Current:     true,
//				CreatedDate: 0,
//				Content:     "Hello!",
//			},
//			err: nil,
//		},
//		{
//			name:   "error response",
//			pageID: "",
//			err:    &types.Error{Kind: types.ErrBadParams, Message: "empty page id"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onCreatePageContent = func(ctx context.Context, pageID string, contents *types.Content) error {
//				return tc.err
//			}
//			gotErr := client.CreatePageContent(context.Background(), tc.pageID, tc.contents)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//
//		})
//	}
//}
//
//func TestGetPage(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name   string
//		pageID string
//		page   *types.Pay
//		err    error
//	}{
//		{
//			name:   "ok response",
//			pageID: "1",
//			page: &types.Pay{
//				ID:       "1",
//				Own:      "site",
//				Name:     "hoop",
//				Contents: nil,
//			},
//			err: nil,
//		},
//		{
//			name:   "error response",
//			pageID: "",
//			err:    &types.Error{Kind: types.ErrBadParams, Message: "empty page id"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetPage = func(ctx context.Context, pageID string) (*types.Pay, error) {
//				return tc.page, tc.err
//			}
//			gotPage, gotErr := client.GetAccount(context.Background(), tc.pageID)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotPage, tc.page) {
//				t.Fatalf("got terminal %#v want %#v", gotPage, tc.page)
//			}
//
//		})
//	}
//}
//
//func TestGetPageWithContentCurrent(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name   string
//		pageID string
//		page   *types.Pay
//		err    error
//	}{
//		{
//			name:   "ok response",
//			pageID: "1",
//			page: &types.Pay{
//				ID:   "1",
//				Own:  "site",
//				Name: "hoop",
//				Contents: []*types.Content{
//					{
//						ID:          "1",
//						Current:     false,
//						CreatedDate: 0,
//						Content:     "World!",
//					},
//					{
//						ID:          "2",
//						Current:     true,
//						CreatedDate: 0,
//						Content:     "Hello!",
//					},
//				},
//			},
//			err: nil,
//		},
//		{
//			name:   "error response",
//			pageID: "",
//			err:    &types.Error{Kind: types.ErrBadParams, Message: "empty page id"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetPageWithContentCurrent = func(ctx context.Context, pageID string) (*types.Pay, error) {
//				return tc.page, tc.err
//			}
//			gotPage, gotErr := client.GetPageWithContentCurrent(context.Background(), tc.pageID)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotPage, tc.page) {
//				t.Fatalf("got terminal %#v want %#v", gotPage, tc.page)
//			}
//
//		})
//	}
//}
//
//func TestGetPageWithContentID(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name      string
//		pageID    string
//		contentID string
//		page      *types.Pay
//		err       error
//	}{
//		{
//			name:      "ok response",
//			pageID:    "1",
//			contentID: "234",
//			page: &types.Pay{
//				ID:   "1",
//				Own:  "site",
//				Name: "hoop",
//				Contents: []*types.Content{
//					{
//						ID:          "123",
//						Current:     false,
//						CreatedDate: 0,
//						Content:     "World!",
//					},
//					{
//						ID:          "234",
//						Current:     true,
//						CreatedDate: 0,
//						Content:     "Hello!",
//					},
//				},
//			},
//			err: nil,
//		},
//		{
//			name:   "error response",
//			pageID: "",
//			err:    &types.Error{Kind: types.ErrBadParams, Message: "empty page id"},
//		},
//		{
//			name:      "error response",
//			pageID:    "1",
//			contentID: "",
//			err:       &types.Error{Kind: types.ErrBadParams, Message: "empty content id"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetPageWithContentID = func(ctx context.Context, pageID, contentID string) (*types.Pay, error) {
//				return tc.page, tc.err
//			}
//			gotPage, gotErr := client.GetPageWithContentID(context.Background(), tc.pageID, tc.contentID)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotPage, tc.page) {
//				t.Fatalf("got terminal %#v want %#v", gotPage, tc.page)
//			}
//
//		})
//	}
//}
//
//func TestGetPageWithContentCreatedDate(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name        string
//		pageID      string
//		CreatedDate string
//		page        *types.Pay
//		err         error
//	}{
//		{
//			name:        "ok response",
//			pageID:      "1",
//			CreatedDate: "234",
//			page: &types.Pay{
//				ID:   "1",
//				Own:  "site",
//				Name: "hoop",
//				Contents: []*types.Content{
//					{
//						ID:          "123",
//						Current:     false,
//						CreatedDate: 0123456,
//						Content:     "World!",
//					},
//					{
//						ID:          "234",
//						Current:     true,
//						CreatedDate: 9876543,
//						Content:     "Hello!",
//					},
//				},
//			},
//			err: nil,
//		},
//		{
//			name:   "error response",
//			pageID: "",
//			err:    &types.Error{Kind: types.ErrBadParams, Message: "empty page id"},
//		},
//		{
//			name:        "error response",
//			pageID:      "1",
//			CreatedDate: "",
//			err:         &types.Error{Kind: types.ErrBadParams, Message: "empty Created date"},
//		},
//		{
//			name:        "error response",
//			pageID:      "1",
//			CreatedDate: "2.1",
//			err:         &types.Error{Kind: types.ErrBadParams, Message: "incorrect Created date"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetPageWithContentCreatedDate = func(ctx context.Context, pageID string, CreatedDate string) (*types.Pay, error) {
//				return tc.page, tc.err
//			}
//			gotPage, gotErr := client.GetPageWithContentCreatedDate(context.Background(), tc.pageID, tc.CreatedDate)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotPage, tc.page) {
//				t.Fatalf("got terminal %#v want %#v", gotPage, tc.page)
//			}
//
//		})
//	}
//}
//
////test node
//func TestCreateNode(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name string
//		node *types.Account
//		err  error
//	}{
//		{
//			name: "ok response",
//			node: &types.Account{
//				ID: "1",
//			},
//			err: nil,
//		},
//		{
//			name: "error response",
//			node: nil,
//			err:  &types.Error{Kind: types.ErrBadParams, Message: "empty node"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onCreateNode = func(ctx context.Context, node *types.Account) (*types.Account, error) {
//				return tc.node, tc.err
//			}
//
//			gotNode, gotErr := client.CreateNode(context.Background(), tc.node)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotNode, tc.node) {
//				t.Fatalf("got terminal %#v want %#v", gotNode, tc.node)
//			}
//
//		})
//	}
//}
//
//func TestDeleteNode(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name  string
//		id    string
//		force bool
//		err   error
//	}{
//		{
//			name:  "ok response",
//			id:    "121211",
//			force: false,
//			err:   nil,
//		},
//		{
//			name:  "error response",
//			id:    "",
//			force: true,
//			err:   &types.Error{Kind: types.ErrBadParams, Message: "empty nodes id"},
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onDeleteNode = func(ctx context.Context, nodeID string, force bool) error {
//				return tc.err
//			}
//
//			gotErr := client.DeleteNode(context.Background(), tc.id, tc.force)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//
//		})
//	}
//}
//
//func TestUpdateNode(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name   string
//		nodeID string
//		node   *types.Account
//		err    error
//	}{}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onUpdateNode = func(ctx context.Context, nodeID string, node *types.Account) (*types.Account, error) {
//				return tc.node, tc.err
//			}
//			gotNode, gotErr := client.UpdateNode(context.Background(), tc.nodeID, tc.node)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotNode, tc.node) {
//				t.Fatalf("got terminal %#v want %#v", gotNode, tc.node)
//			}
//
//		})
//	}
//}
//
//func TestGetNodeById(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name   string
//		nodeID string
//		node   *types.Account
//		err    error
//	}{}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetNodeByID = func(ctx context.Context, nodeID string) (*types.Account, error) {
//				return tc.node, tc.err
//			}
//			gotNode, gotErr := client.GetNode(context.Background(), tc.nodeID)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotNode, tc.node) {
//				t.Fatalf("got terminal %#v want %#v", gotNode, tc.node)
//			}
//
//		})
//	}
//}
//
//func TestGetNodeChildrenById(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name   string
//		nodeID string
//		nodes  []*types.Account
//		err    error
//	}{}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetNodeChildrenByID = func(ctx context.Context, nodeID string) ([]*types.Account, error) {
//				return tc.nodes, tc.err
//			}
//			gotNode, gotErr := client.GetNodeChildrenById(context.Background(), tc.nodeID)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotNode, tc.nodes) {
//				t.Fatalf("got terminal %#v want %#v", gotNode, tc.nodes)
//			}
//
//		})
//	}
//}
//
////test site
//func TestCreateSite(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name string
//		site *types.Site
//		err  error
//	}{
//		{
//			name: "ID exists",
//			site: &types.Site{
//				ID:             "1",
//				Name:           "наш сайт",
//				Domains:        nil,
//				DeliveryConfig: "",
//				Root:           "",
//			},
//			err: nil,
//		},
//		{
//			name: "ID not exists",
//			site: &types.Site{
//				ID:   "1",
//				Name: "наш сайт",
//			},
//			err: nil,
//		},
//		{
//			name: "error site not exist",
//			site: nil,
//			err:  types.Errorf(types.ErrBadParams, "site is nil"),
//		},
//	}
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onCreateSite = func(ctx context.Context, site *types.Site) (*types.Site, error) {
//				return tc.site, tc.err
//			}
//
//			gotNode, gotErr := client.CreateSite(context.Background(), tc.site)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotNode, tc.site) {
//				t.Fatalf("got terminal %#v want %#v", gotNode, tc.site)
//			}
//
//		})
//	}
//}
//
//func TestUpdateSite(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name   string
//		siteID string
//		site   *types.Site
//		err    error
//	}{
//		{
//			name:   "OK test",
//			siteID: "1",
//			site: &types.Site{ID: "1",
//				Name:           "наш сайт",
//				Domains:        nil,
//				DeliveryConfig: "",
//				Root:           "",
//			},
//			err: nil,
//		},
//		{
//			name:   "error site id is empty",
//			siteID: "",
//			site: &types.Site{ID: "1",
//				Name:           "наш сайт",
//				Domains:        nil,
//				DeliveryConfig: "",
//				Root:           "",
//			},
//			err: types.Errorf(types.ErrBadParams, "empty site id"),
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onUpdateSite = func(ctx context.Context, nodeID string, node *types.Site) (*types.Site, error) {
//				return tc.site, tc.err
//			}
//			gotSite, gotErr := client.UpdateSite(context.Background(), tc.siteID, tc.site)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotSite, tc.site) {
//				t.Fatalf("got terminal %#v want %#v", gotSite, tc.site)
//			}
//
//		})
//	}
//}
//
//func TestDeleteSite(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name string
//		id   string
//		err  error
//	}{
//		{
//			name: "ok response",
//			id:   "121211",
//			err:  nil,
//		},
//		{
//			name: "error response",
//			id:   "",
//			err:  types.Errorf(types.ErrBadParams, "empty site id"),
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onDeleteSite = func(ctx context.Context, nodeID string) error {
//				return tc.err
//			}
//
//			gotErr := client.DeleteSite(context.Background(), tc.id)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//
//		})
//	}
//}
//
//func TestGetSite(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name   string
//		siteID string
//		site   *types.Site
//		err    error
//	}{
//		{
//			name:   "OK test",
//			siteID: "1",
//			site: &types.Site{ID: "1",
//				Name:           "наш сайт",
//				Domains:        nil,
//				DeliveryConfig: "",
//				Root:           "",
//			},
//			err: nil,
//		},
//		{
//			name:   "types.Error Site ID empty",
//			siteID: "",
//			site:   nil,
//			err:    types.Errorf(types.ErrBadParams, "empty site id"),
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetSite = func(ctx context.Context, siteID string) (*types.Site, error) {
//				return tc.site, tc.err
//			}
//			gotSite, gotErr := client.GetSite(context.Background(), tc.siteID)
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotSite, tc.site) {
//				t.Fatalf("got terminal %#v want %#v", gotSite, tc.site)
//			}
//
//		})
//	}
//}
//
//func TestGetSiteList(t *testing.T) {
//	server, client, svc := startTestServer(t)
//	defer server.Close()
//
//	testCases := []struct {
//		name  string
//		sites []*types.Site
//		err   error
//	}{
//		{
//			name: "Get sites",
//			sites: []*types.Site{{ID: "1",
//				Name:           "наш сайт",
//				Domains:        nil,
//				DeliveryConfig: "",
//				Root:           "",
//			},
//			},
//			err: nil,
//		},
//	}
//
//	for _, tc := range testCases {
//		t.Run(tc.name, func(t *testing.T) {
//			svc.onGetSiteList = func(ctx context.Context) ([]*types.Site, error) {
//				return tc.sites, tc.err
//			}
//			gotSite, gotErr := client.GetSiteList(context.Background())
//			if !reflect.DeepEqual(gotErr, tc.err) {
//				t.Fatalf("got error %#v want %#v", gotErr, tc.err)
//			}
//			if !reflect.DeepEqual(gotSite, tc.sites) {
//				t.Fatalf("got terminal %#v want %#v", gotSite, tc.sites)
//			}
//
//		})
//	}
//}
//*/
