package httpserver

import (
	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics/expvar"
	opentracing2 "github.com/go-kit/kit/tracing/opentracing"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus/promhttp"
	paymentEndpoint "gitlab.com/r1se/payment-service/endpoint"
	paymentMiddleware "gitlab.com/r1se/payment-service/middleware"
	paymentService "gitlab.com/r1se/payment-service/service"
	paymentTransport "gitlab.com/r1se/payment-service/transport"
	"golang.org/x/time/rate"
	"net/http"
)

type handlerConfig struct {
	svc         paymentService.Service
	logger      log.Logger
	tracer      opentracing.Tracer
	rateLimiter *rate.Limiter
	apiVersion  string
}

// newHandlers creates a new HTTP handler serving service endpoints.
func newHandlers(cfg *handlerConfig) http.Handler {
	svc := &paymentMiddleware.LoggingMiddleware{Service: cfg.svc, Logger: cfg.logger,
		RequestCount:   expvar.NewCounter("count_handler"),
		RequestLatency: expvar.NewHistogram("latency_handler", 50)}

	// Account service endpoint

	createAccountEndpoint := paymentEndpoint.MakeCreateAccountEndpoint(svc)
	createAccountEndpoint = applyMiddleware(createAccountEndpoint, "CreateAccount", cfg)

	deleteAccountEndpoint := paymentEndpoint.MakeDeleteAccountEndpoint(svc)
	deleteAccountEndpoint = applyMiddleware(deleteAccountEndpoint, "DeleteAccount", cfg)

	updateAccountEndpoint := paymentEndpoint.MakeUpdateAccountEndpoint(svc)
	updateAccountEndpoint = applyMiddleware(updateAccountEndpoint, "UpdateAccount", cfg)

	getAccountEndpoint := paymentEndpoint.MakeGetAccountEndpoint(svc)
	getAccountEndpoint = applyMiddleware(getAccountEndpoint, "GetAccount", cfg)

	getAccountsListEndpoint := paymentEndpoint.MakeGetAccountsListEndpoint(svc)
	getAccountsListEndpoint = applyMiddleware(getAccountsListEndpoint, "GetAccountsList", cfg)

	// Pay service endpoint

	createPayEndpoint := paymentEndpoint.MakeCreatePayEndpoint(svc)
	createPayEndpoint = applyMiddleware(createPayEndpoint, "CreatePay", cfg)

	getPayEndpoint := paymentEndpoint.MakeGetPayEndpoint(svc)
	getPayEndpoint = applyMiddleware(getPayEndpoint, "GetPay", cfg)

	getPaymentsEndpoint := paymentEndpoint.MakeGetPaymentsEndpoint(svc)
	getPaymentsEndpoint = applyMiddleware(getPaymentsEndpoint, "GetPaymentsList", cfg)

	router := mux.NewRouter()

	// Account service

	router.Path(cfg.apiVersion + "/account_list").Methods("POST").Handler(kithttp.NewServer(
		getAccountsListEndpoint,
		paymentTransport.DecodeGetNodesRequest,
		paymentTransport.EncodeGetAccountsResponse,
	))

	router.Path(cfg.apiVersion + "/account").Methods("POST").Handler(kithttp.NewServer(
		createAccountEndpoint,
		paymentTransport.DecodeCreateAccountRequest,
		paymentTransport.EncodeCreateAccountResponse,
	))

	router.Path(cfg.apiVersion + "/account").Methods("PUT").Handler(kithttp.NewServer(
		updateAccountEndpoint,
		paymentTransport.DecodeUpdateAccountRequest,
		paymentTransport.EncodeUpdateAccountResponse,
	))

	router.Path(cfg.apiVersion + "/account/{account_name}").Methods("GET").Handler(kithttp.NewServer(
		getAccountEndpoint,
		paymentTransport.DecodeGetAccountRequest,
		paymentTransport.EncodeGetAccountResponse,
	))

	router.Path(cfg.apiVersion + "/account/{account_name}").Methods("DELETE").Handler(kithttp.NewServer(
		deleteAccountEndpoint,
		paymentTransport.DecodeDeleteAccountRequest,
		paymentTransport.EncodeDeleteAccountResponse,
	))

	// Pay service
	router.Path(cfg.apiVersion + "/payment_list").Methods("POST").Handler(kithttp.NewServer(
		getPaymentsEndpoint,
		paymentTransport.DecodeGetPaymentsRequest,
		paymentTransport.EncodeGetPaymentsResponse,
	))

	router.Path(cfg.apiVersion + "/payment/{payment_id}").Methods("GET").Handler(kithttp.NewServer(
		getPayEndpoint,
		paymentTransport.DecodeGetPayRequest,
		paymentTransport.EncodeGetPayResponse,
	))

	router.Path(cfg.apiVersion + "/pay").Methods("POST").Handler(kithttp.NewServer(
		createPayEndpoint,
		paymentTransport.DecodeCreatePayRequest,
		paymentTransport.EncodeCreatePayResponse,
	))

	router.Path("/metrics").Methods("GET").Handler(stdprometheus.Handler())

	router.Path("/docs/public.yaml").Methods("GET").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./api/payment-service/public-v1.yaml")
	})
	router.PathPrefix("/docs/").Handler(http.StripPrefix("/docs/",
		http.FileServer(http.Dir("./web/static/swagger-ui/"))))

	return router
}

func applyMiddleware(e endpoint.Endpoint, method string, cfg *handlerConfig) endpoint.Endpoint {
	if cfg.tracer != nil {
		e = opentracing2.TraceServer(cfg.tracer, method)(e)
	}
	return e
}
