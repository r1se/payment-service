package paymentdb

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/pressly/goose"
	"github.com/stretchr/testify/assert"
	types "gitlab.com/r1se/payment-service/types/payment"
	"os"
	"testing"
)

var logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
var sqlDB *sqlx.DB

func TestStorage_Account(t *testing.T) {
	ctx := context.Background()
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	storage := setUp(t)

	_, err := sqlDB.Exec("DELETE FROM accounts WHERE account = $1", "test123")
	assert.NoError(t, err)

	_, err = sqlDB.Exec("DELETE FROM accounts WHERE account = $1", "test456")
	assert.NoError(t, err)

	_, err = storage.CreateAccount(ctx, &types.Account{Account: "test123", Amount: 123.45, Currency: 1})
	assert.NoError(t, err)

	acc, err := storage.GetAccount(ctx, "test123")
	assert.NoError(t, err)
	assert.Equal(t, "test123", acc.Account)

	_, err = storage.CreateAccount(ctx, &types.Account{Account: "test456", Amount: 981.27, Currency: 1})
	assert.NoError(t, err)

	accs, err := storage.GetAccounts(ctx, 0, 10)
	assert.NoError(t, err)
	assert.Equal(t, 4, len(accs.Accounts))

	updAcc, err := storage.UpdateAccount(ctx, "test456", &types.Account{Amount: 111.11})
	assert.NoError(t, err)
	assert.Equal(t, 111.11, updAcc.Amount)
	assert.Equal(t, false, updAcc.Active)

	err = storage.DeleteAccount(ctx, "test123")
	assert.NoError(t, err)

	err = storage.DeleteAccount(ctx, "test456")
	assert.NoError(t, err)
}

func setUp(t *testing.T) *Storage {
	err := godotenv.Load("../../.env")
	if err != nil {
		fmt.Println(err)
	}
	dsn := buildConnectionString(logger)

	sqlDB, err = sqlx.Open("postgres", dsn)
	if err != nil {
		t.Fatal("Can't connect to db ", err)
	}

	err = goose.Up(sqlDB.DB, "../../migrations")
	if err != nil {
		t.Fatal("Can't up migrations", err)
	}

	storage, err := New(logger, sqlDB)
	if err != nil {
		t.Fatal("failed to initialize storage", err)
	}

	return storage
}

func buildConnectionString(logger log.Logger) string {
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	if user == "" || pass == "" {
		panic("You must include POSTGRES_USER and POSTGRES_PASSWORD environment variables")
	}
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dbname := os.Getenv("POSTGRES_DB")
	if host == "" || port == "" || dbname == "" {
		panic("You must include POSTGRES_HOST, POSTGRES_PORT, and POSTGRES_DB environment variables")
	}

	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, pass, host, port, dbname)
}
