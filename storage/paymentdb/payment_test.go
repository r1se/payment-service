package paymentdb

import (
	"context"
	"github.com/go-kit/kit/log"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	types "gitlab.com/r1se/payment-service/types/payment"
	"testing"
)

func TestStorage_CreatePay(t *testing.T) {
	ctx := context.Background()
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	storage := setUp(t)

	_, err := sqlDB.Exec("DELETE FROM accounts WHERE account = $1", "test123")
	assert.NoError(t, err)

	_, err = sqlDB.Exec("DELETE FROM accounts WHERE account = $1", "test456")
	assert.NoError(t, err)

	_, err = storage.CreateAccount(ctx, &types.Account{Account: "test123", Amount: 123.45, Currency: 1})
	assert.NoError(t, err)

	acc1, err := storage.GetAccount(ctx, "test123")
	assert.NoError(t, err)
	assert.Equal(t, "test123", acc1.Account)

	_, err = storage.CreateAccount(ctx, &types.Account{Account: "test456", Amount: 981.27, Currency: 1})
	assert.NoError(t, err)

	acc2, err := storage.GetAccount(ctx, "test456")
	assert.NoError(t, err)
	assert.Equal(t, "test456", acc2.Account)

	transactionID := uuid.New()
	err = storage.CreatePay(ctx, transactionID.String(), &types.PayRequest{Amount: 100.13, FromAccount: acc2.Account, ToAccount: acc1.Account, Currency: 1}, false)
	assert.NoError(t, err)

	pay, err := storage.GetPay(ctx, transactionID.String())
	assert.NoError(t, err)
	assert.Equal(t, acc2.Account, pay.FromAccount)
	assert.Equal(t, acc1.Account, pay.ToAccount)

	err = storage.UpdatePayComments(ctx, transactionID.String(), "test")
	assert.NoError(t, err)

	pay, err = storage.GetPay(ctx, transactionID.String())
	assert.NoError(t, err)
	assert.Equal(t, "test", pay.ErrorReason)

	secondTransactionID := uuid.New()
	err = storage.CreatePay(ctx, secondTransactionID.String(), &types.PayRequest{Amount: 99.13, FromAccount: acc2.Account, ToAccount: acc1.Account, Currency: 1}, false)
	assert.NoError(t, err)

	payments, err := storage.GetPayments(ctx, 0, 10)
	assert.NoError(t, err)
	assert.Equal(t, true, len(payments.Payments) > 0)
}
