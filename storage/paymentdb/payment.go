package paymentdb

import (
	"context"
	"database/sql"
	"github.com/shopspring/decimal"
	types "gitlab.com/r1se/payment-service/types/payment"
	"time"
)

// CreateAccount creates a new page
func (s *Storage) CreatePay(ctx context.Context, transactionID string, payInfo *types.PayRequest, isBroken bool) error {
	var outAmount, inAmount float64
	//add log info
	if !isBroken {
		_, err := s.dbConnect.ExecContext(ctx, `INSERT INTO pay_operations (transaction_id, account_from, "account_to", "operation", amount, currency, is_ended)
					VALUES ($1, $2, $3, $4, $5, $6, $7)`, transactionID, payInfo.FromAccount, payInfo.ToAccount, 1, payInfo.Amount, payInfo.Currency, false)
		if err != nil {
			return err
		}
	}

	//lock account to update
	tx := s.dbConnect.MustBegin()
	row := tx.QueryRowContext(ctx,
		`SELECT outAcc.amount, inAcc.amount FROM accounts as outAcc, accounts as inAcc WHERE
                outAcc.account = $1 AND inAcc.account = $2
                  AND inAcc.active = true AND outAcc.active = true 
                  AND outAcc.Amount > $3 AND outAcc.currency = inAcc.currency for update;`,
		payInfo.FromAccount, payInfo.ToAccount, payInfo.Amount)
	err := row.Scan(&outAmount, &inAmount)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
		return err
	}

	// update balance on accounts
	newAmountOut, _ := decimal.NewFromFloat(outAmount).Sub(decimal.NewFromFloat(payInfo.Amount)).Float64()
	_, err = tx.ExecContext(ctx, `UPDATE accounts SET amount=$1 WHERE account = $2`,
		newAmountOut, payInfo.FromAccount)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
		return err
	}

	newAmountIn, _ := decimal.NewFromFloat(inAmount).Add(decimal.NewFromFloat(payInfo.Amount)).Float64()
	_, err = tx.ExecContext(ctx, `UPDATE accounts SET amount=$1 WHERE account = $2`,
		newAmountIn, payInfo.ToAccount)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
		return err
	}

	//set outgoing to true and add incoming operations
	_, err = tx.ExecContext(ctx, `UPDATE pay_operations SET "is_ended"=true, updated_at=$1, error_info='' WHERE "transaction_id" = $2 AND "operation" = 1`,
		time.Now(), transactionID)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
		return err
	}
	needInsert := true
	if isBroken {
		var existTransactionID string
		err = tx.GetContext(ctx, &existTransactionID, `SELECT transaction_id FROM pay_operations WHERE "transaction_id" = $1 AND "operation" = 2`, transactionID)
		if err != sql.ErrNoRows {
			needInsert = false
		}
	}
	if needInsert {
		_, err = tx.ExecContext(ctx, `INSERT INTO pay_operations ("transaction_id", account_from, "account_to", "operation", amount, currency, "is_ended", updated_at)
					VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`, transactionID, payInfo.FromAccount, payInfo.ToAccount, 2, payInfo.Amount, payInfo.Currency, true, time.Now())
		if err != nil {
			if !isBroken {
				if rollbackErr := tx.Rollback(); rollbackErr != nil {
					return rollbackErr
				}
				return err
			}
		}
	}

	err = tx.Commit()

	return err
}

func (s *Storage) GetPay(ctx context.Context, paymentID string) (*types.Payment, error) {
	payment := &types.Payment{}
	err := s.dbConnect.GetContext(ctx, payment, `SELECT transaction_id as paymentid,  account_from as fromaccount,
       "account_to" as toaccount , "operation" as paymenttype, amount, currency, "is_ended" as isended, error_info as errorreason
		FROM pay_operations WHERE "transaction_id" = $1 AND "is_ended" = true ORDER BY updated_at LIMIT 1`,
		paymentID)

	return payment, err
}

func (s *Storage) GetPayments(ctx context.Context, pageNum, pageSize int) (*types.Payments, error) {
	if pageNum == 1 {
		pageNum = 0
	}
	payments := []*types.Payment{}
	err := s.dbConnect.SelectContext(ctx, &payments, `SELECT transaction_id as paymentid,  account_from as fromaccount,
	"account_to" as toaccount , "operation" as paymenttype, amount, currency, "is_ended" as isended FROM pay_operations LIMIT $1 OFFSET $2`,
		pageSize, pageSize*pageNum)

	return &types.Payments{Payments: payments}, err
}
func (s *Storage) UpdatePayComments(ctx context.Context, paymentID, errMsg string) error {
	tx := s.dbConnect.MustBegin()
	_, err := tx.ExecContext(ctx, `UPDATE pay_operations SET "error_info"=$1 WHERE  "transaction_id" = $2`,
		errMsg, paymentID)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
	} else {
		err = tx.Commit()
	}
	return err
}

func (s *Storage) GetBadPay(ctx context.Context) (*types.Payments, error) {
	payments := []*types.Payment{}

	tx := s.dbConnect.MustBegin()
	err := tx.SelectContext(ctx, &payments, `SELECT transaction_id as paymentid,  account_from as fromaccount,
	"account_to" as toaccount , "operation" as paymenttype, amount, currency, "is_ended" as isended FROM pay_operations WHERE operation = 1 AND is_ended=false AND error_info!=''`)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return nil, rollbackErr
		}
	} else {
		err = tx.Commit()
	}
	return &types.Payments{Payments: payments}, err
}
