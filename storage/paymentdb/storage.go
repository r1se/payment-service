package paymentdb

import (
	"github.com/go-kit/kit/log"
	"github.com/jmoiron/sqlx"
)

type Storage struct {
	logger    log.Logger
	dbConnect *sqlx.DB
}

// New creates a new postgres storage using the given configuration.
func New(logger log.Logger, database *sqlx.DB) (*Storage, error) {
	return &Storage{
		logger:    logger,
		dbConnect: database,
	}, nil
}

// Shutdown close postgres session
func (s *Storage) Shutdown() {
	s.dbConnect.Close()
}
