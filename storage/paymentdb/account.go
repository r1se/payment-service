package paymentdb

import (
	"context"
	"database/sql"
	types "gitlab.com/r1se/payment-service/types/payment"
)

func (s *Storage) CreateAccount(ctx context.Context, accountInfo *types.Account) (*types.Account, error) {
	tx := s.dbConnect.MustBegin()
	_, err := tx.ExecContext(ctx, "INSERT INTO accounts (account, currency, amount) VALUES ($1, $2, $3)",
		accountInfo.Account, accountInfo.Currency, accountInfo.Amount)
	if err != nil {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return nil, rollbackErr
		}
	} else {
		err = tx.Commit()
	}
	return accountInfo, err
}

func (s *Storage) GetAccount(ctx context.Context, accountName string) (*types.Account, error) {
	account := &types.Account{}
	err := s.dbConnect.GetContext(ctx, account, "SELECT account, amount, currency, active FROM accounts WHERE account = $1",
		accountName)

	return account, err
}

func (s *Storage) GetAccounts(ctx context.Context, pageNum, pageSize int) (*types.Accounts, error) {
	accounts := []*types.Account{}
	err := s.dbConnect.SelectContext(ctx, &accounts, "SELECT account, amount, currency, active FROM accounts LIMIT $1 OFFSET $2",
		pageSize, pageSize*pageNum)

	return &types.Accounts{Accounts: accounts}, err
}

func (s *Storage) UpdateAccount(ctx context.Context, accountName string, accountInfo *types.Account) (*types.Account, error) {
	tx := s.dbConnect.MustBegin()
	accountInfo.Account = accountName
	result, err := tx.ExecContext(ctx, "UPDATE accounts SET amount=$1, active = $2 WHERE account = $3 returning id",
		accountInfo.Amount, accountInfo.Active, accountName)
	rowsAffected, _ := result.RowsAffected()
	if err != nil || rowsAffected == 0 {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return nil, rollbackErr
		}
		if rowsAffected == 0 {
			return nil, sql.ErrNoRows
		}
	} else {
		err = tx.Commit()
	}
	return accountInfo, err
}

func (s *Storage) DeleteAccount(ctx context.Context, accountName string) error {
	tx := s.dbConnect.MustBegin()
	result, _ := tx.ExecContext(ctx, "UPDATE accounts SET active=false, deleted=true WHERE account = $1 returning id",
		accountName)
	rowsAffected, err := result.RowsAffected()
	if err != nil || rowsAffected == 0 {
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			return rollbackErr
		}
		if rowsAffected == 0 {
			return sql.ErrNoRows
		}
	} else {
		err = tx.Commit()
	}
	return err
}
