package storage

import (
	"context"
	types "gitlab.com/r1se/payment-service/types/payment"
)

// Storage is a service storage.
type Storage interface {
	CreateAccount(ctx context.Context, accountInfo *types.Account) (*types.Account, error)
	GetAccount(ctx context.Context, accountName string) (*types.Account, error)
	GetAccounts(ctx context.Context, pageNum, pageSize int) (*types.Accounts, error)
	UpdateAccount(ctx context.Context, accountName string, accountInfo *types.Account) (*types.Account, error)
	DeleteAccount(ctx context.Context, accountName string) error

	CreatePay(ctx context.Context, transactionID string, payInfo *types.PayRequest, isBroken bool) error
	GetPay(ctx context.Context, paymentID string) (*types.Payment, error)
	GetPayments(ctx context.Context, pageNum, pageSize int) (*types.Payments, error)
	UpdatePayComments(ctx context.Context, paymentID, errMsg string) error
	GetBadPay(ctx context.Context) (*types.Payments, error)
}
