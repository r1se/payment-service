# Base image for building the go project
FROM tetafro/golang-gcc:1.14-alpine AS build

# Updates the repository and installs git
RUN apk update && apk upgrade && \
    apk add --no-cache git


# Get tool for migrations
RUN go get -u github.com/pressly/goose/cmd/goose

# Switches to /tmp/app as the working directory, similar to 'cd'
WORKDIR /tmp/app

COPY . .

# Builds the current project to a binary file called paymentapp
# The location of the binary file is /tmp/app/out/paymentapp
RUN GOOS=linux go build -o ./out/paymentapp ./cmd/payment-service

#########################################################

# The project has been successfully built and we will use a
# lightweight alpine image to run the server
FROM alpine:latest

# Adds CA Certificates to the image
RUN apk add ca-certificates

# Copies the binary file from the BUILD container to /app folder
COPY --from=build /tmp/app/out/paymentapp /app/paymentapp
COPY --from=build /tmp/app/web /app/web/
COPY --from=build /tmp/app/api /app/api/
COPY --from=build /tmp/app/migrations /app/migrations/
COPY --from=build /tmp/app/migrate.sh /app/migrate.sh
COPY --from=build /go/bin/goose /usr/local/bin/goose


# Switches working directory to /app
WORKDIR "/app"

# Exposes the 5000 port from the container
EXPOSE 5000

ENV WAIT_VERSION 2.7.2
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/$WAIT_VERSION/wait /wait
RUN chmod +x /wait
RUN chmod +x ./migrate.sh

# Runs the binary once the container starts
CMD ["./paymentapp"]