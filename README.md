payment-service
=

## Общее описание
Сервис  перевода средств клиентов.
Подробная документация доступна в подпаке ```docs```

### Контракт
Для описания API используется файл `api/payment-service/public-v1.yaml`.

Чтобы избежать несоответствия API и handler-ов используется генерации моделей go-swagger(https://goswagger.io/install.html)

Модели генерируются командой:

`swagger generate model -f ./api/payment-service/public-v1.yaml -t ./types/ -m payment
`
### Запуск в контейнере

`docker-compose --compatibility up --build`

Сервис доступен по адресу http://localhost:82/

Документация по адресу http://localhost:82/docs/

### Настройка для разработчиков

- Создайте файл `.env`. В качестве основы - `.env.example` 

- Подготовте локальную базу данных:

```docker run --name postgres -e POSTGRES_PASSWORD=postgres -d -p 54320:5432 -v pgdata:/var/lib/postgresql/data postgres:9.6```

```docker exec -i postgres psql -U postgres -c "CREATE DATABASE payment WITH ENCODING='UTF8' OWNER=postgres;"```

```go get -u github.com/pressly/goose/cmd/goose```

```./migrate.sh --localhost```
- Запустите приложение ```go run ./cmd/payment-service/main.go```

Сервис доступен по адресу http://localhost:5000/

Документация по адресу http://localhost:5000/docs/

**!NB** не забывайте проверять host в файле ```./api/payment-service/public-v1.yaml```