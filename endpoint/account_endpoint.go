package endpoint

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"gitlab.com/r1se/payment-service/service"
	types "gitlab.com/r1se/payment-service/types/payment"
)

func MakeCreateAccountEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateAccountRequest)
		account, err := svc.CreateAccount(ctx, req.Account)
		return CreateAccountResponse{Account: account, Err: err}, nil
	}
}

type CreateAccountRequest struct {
	Account *types.Account
}

type CreateAccountResponse struct {
	Account *types.Account
	Err     error
}

func MakeDeleteAccountEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(DeleteAccountRequest)
		err := svc.DeleteAccount(ctx, req.AccountName)
		return DeleteAccountResponse{Err: err}, nil
	}
}

type DeleteAccountRequest struct {
	AccountName string
}

type DeleteAccountResponse struct {
	Err error
}

func MakeUpdateAccountEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(UpdateAccountRequest)
		account, err := svc.UpdateAccount(ctx, req.AccountReq.AccountName, req.AccountReq.Account)
		return UpdateAccountResponse{Account: account, Err: err}, nil
	}
}

type UpdateAccountRequest struct {
	AccountReq *types.AccountRequest
}

type UpdateAccountResponse struct {
	Account *types.Account
	Err     error
}

func MakeGetAccountEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetAccountRequest)
		account, err := svc.GetAccount(ctx, req.AccountName)
		return GetAccountResponse{Account: account, Err: err}, nil
	}
}

type GetAccountRequest struct {
	AccountName string
}

type GetAccountResponse struct {
	Account *types.Account
	Err     error
}

func MakeGetAccountsListEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetAccountsRequest)
		accounts, err := svc.GetAccountsList(ctx, req.PageNum, req.PageSize)
		return GetAccountsResponse{Accounts: accounts, Err: err}, nil
	}
}

type GetAccountsRequest struct {
	PageNum  int
	PageSize int
}

type GetAccountsResponse struct {
	Accounts *types.Accounts
	Err      error
}
