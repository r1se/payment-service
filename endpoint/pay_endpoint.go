package endpoint

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"gitlab.com/r1se/payment-service/service"
	types "gitlab.com/r1se/payment-service/types/payment"
)

func MakeCreatePayEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreatePayRequest)
		err := svc.CreatePay(ctx, req.Pay)
		return CreatePayResponse{Err: err}, nil
	}
}

type CreatePayRequest struct {
	Pay *types.PayRequest
}

type CreatePayResponse struct {
	Err error
}

func MakeGetPayEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPayRequest)
		payment, err := svc.GetPayment(ctx, req.PayUUID)
		return GetPayResponse{Pay: payment, Err: err}, nil
	}
}

type GetPayRequest struct {
	PayUUID string
}

type GetPayResponse struct {
	Pay *types.Payment
	Err error
}

func MakeGetPaymentsEndpoint(svc service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(GetPaymentsRequest)
		payments, err := svc.GetPaymentsList(ctx, req.PageNum, req.PageSize)
		return GetPaymentsResponse{Payments: payments, Err: err}, nil
	}
}

type GetPaymentsRequest struct {
	PageNum  int
	PageSize int
}

type GetPaymentsResponse struct {
	Payments *types.Payments
	Err      error
}
