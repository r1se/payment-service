package service

import (
	"context"
	"github.com/go-kit/kit/log"
	"github.com/stretchr/testify/assert"
	. "gitlab.com/r1se/payment-service/service/mocks"
	types "gitlab.com/r1se/payment-service/types/payment"
	"os"
	"testing"
)

var logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))

func Test_service_CreateAccount(t *testing.T) {
	ctx := context.Background()
	a := assert.New(t)
	storageMock := NewStorageMock(t)
	service := NewService(storageMock, logger, nil)
	acc1 := &types.Account{Account: "mock1", Active: true, Amount: 100.50, Currency: 1}
	storageMock.CreateAccountMock.Expect(ctx, acc1).Return(acc1, nil)
	retAcc, err := service.CreateAccount(ctx, acc1)
	a.NoError(err)
	a.Equal(retAcc, acc1)
}
