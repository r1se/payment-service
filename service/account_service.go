package service

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log/level"

	types "gitlab.com/r1se/payment-service/types/payment"
)

func (s *service) CreateAccount(ctx context.Context, account *types.Account) (*types.Account, error) {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("CreateAccount")
		defer span.Finish()
	}
	account, err := s.Storage.CreateAccount(ctx, account)
	if err != nil {
		_ = level.Error(s.Logger).Log("accountService/CreateAccount error: ", err.Error())
		return nil, fmt.Errorf("accountService/CreateAccount error:: " + err.Error())
	}
	return account, nil
}

func (s *service) DeleteAccount(ctx context.Context, accountName string) error {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("DeleteAccount")
		defer span.Finish()
	}
	err := s.Storage.DeleteAccount(ctx, accountName)
	if err != nil {
		_ = level.Error(s.Logger).Log("accountService/DeleteAccount error: ", err.Error())
		return fmt.Errorf("accountService/DeleteAccount error: " + err.Error())
	}
	return nil
}

func (s *service) UpdateAccount(ctx context.Context, accountName string, account *types.Account) (*types.Account, error) {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("UpdateAccount")
		defer span.Finish()
	}
	updAccount, err := s.Storage.UpdateAccount(ctx, accountName, account)
	if err != nil {
		_ = level.Error(s.Logger).Log("accountService/UpdateAccount error: ", err.Error())
		return nil, fmt.Errorf("accountService/UpdateAccount error: " + err.Error())
	}
	return updAccount, nil
}

func (s *service) GetAccount(ctx context.Context, accountName string) (*types.Account, error) {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("GetAccount")
		defer span.Finish()
	}
	account, err := s.Storage.GetAccount(ctx, accountName)
	if err != nil {
		_ = level.Error(s.Logger).Log("accountService/GetAccount error: ", err.Error())
		return nil, fmt.Errorf("accountService/GetAccount error: " + err.Error())
	}
	return account, nil
}

func (s *service) GetAccountsList(ctx context.Context, pageNum, pageSize int) (*types.Accounts, error) {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("GetAccountsList")
		defer span.Finish()
	}
	if pageNum == 1 {
		pageNum = 0
	}
	accounts, err := s.Storage.GetAccounts(ctx, pageNum, pageSize)
	if err != nil {
		_ = level.Error(s.Logger).Log("accountService/GetAccountsList error: ", err.Error())
		return nil, fmt.Errorf("accountService/GetAccountsList error: " + err.Error())
	}
	return accounts, nil
}
