package service

import (
	"context"
	"github.com/stretchr/testify/assert"
	. "gitlab.com/r1se/payment-service/service/mocks"
	types "gitlab.com/r1se/payment-service/types/payment"
	"testing"
)

func Test_service_CreatePay(t *testing.T) {
	ctx := context.Background()
	a := assert.New(t)
	storageMock := NewStorageMock(t)
	service := NewService(storageMock, logger, nil)
	pay1 := &types.PayRequest{FromAccount: "mock1", ToAccount: "mock2", Amount: 100.50, Currency: 1}
	storageMock.CreatePayMock.Return(nil)
	err := service.CreatePay(ctx, pay1)
	a.NoError(err)
}
