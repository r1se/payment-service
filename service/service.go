package service

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/jmoiron/sqlx"
	"github.com/opentracing/opentracing-go"
	"gitlab.com/r1se/payment-service/storage"
	"gitlab.com/r1se/payment-service/storage/paymentdb"
	types "gitlab.com/r1se/payment-service/types/payment"
	"os"
)

// service manages HTTP server methods.
type Service interface {

	// Account service
	CreateAccount(ctx context.Context, account *types.Account) (*types.Account, error)
	DeleteAccount(ctx context.Context, accountName string) error
	UpdateAccount(ctx context.Context, accountName string, account *types.Account) (*types.Account, error)
	GetAccount(ctx context.Context, accountName string) (*types.Account, error)
	GetAccountsList(ctx context.Context, pageNum, pageSize int) (*types.Accounts, error)

	// Payment service
	GetPaymentsList(ctx context.Context, pageNum, pageSize int) (*types.Payments, error)
	GetPayment(ctx context.Context, payUUID string) (*types.Payment, error)
	CreatePay(ctx context.Context, payRequest *types.PayRequest) error
}

type service struct {
	Logger  log.Logger
	Storage storage.Storage
	Tracing opentracing.Tracer
}

func NewService(storage storage.Storage, logger log.Logger, tracing opentracing.Tracer) Service {
	return &service{Storage: storage, Logger: logger, Tracing: tracing}
}

func NewTestService() Service {
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	dsn := buildConnectionString(logger)
	database, err := sqlx.Open("postgres", dsn)
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed open connection to database", "err", err)
		os.Exit(1)
	}
	err = database.Ping()
	if err != nil {
		_ = level.Error(logger).Log("msg", "failed to ping database", "err", err)
		os.Exit(1)
	}
	dbStorage, err := paymentdb.New(logger, database)
	if err != nil {
		panic(err)
	}
	return NewService(dbStorage, logger, nil)
}

func buildConnectionString(logger log.Logger) string {
	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	if user == "" || pass == "" {
		_ = level.Error(logger).Log("msg", "You must include POSTGRES_USER and POSTGRES_PASSWORD environment variables")
	}
	host := os.Getenv("POSTGRES_HOST")
	port := os.Getenv("POSTGRES_PORT")
	dbname := os.Getenv("POSTGRES_DB")
	if host == "" || port == "" || dbname == "" {
		_ = level.Error(logger).Log("msg", "You must include POSTGRES_HOST, POSTGRES_PORT, and POSTGRES_DB environment variables")
	}

	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, pass, host, port, dbname)
}
