package service

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/log/level"
	"github.com/google/uuid"

	types "gitlab.com/r1se/payment-service/types/payment"
)

func (s *service) GetPaymentsList(ctx context.Context, pageNum, pageSize int) (*types.Payments, error) {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("GetPaymentsList")
		defer span.Finish()
	}
	if pageNum == 1 {
		pageNum = 0
	}
	payments, err := s.Storage.GetPayments(ctx, pageNum, pageSize)
	if err != nil {
		_ = level.Error(s.Logger).Log("paymentService/GetPaymentsList error: ", err.Error())
		return nil, fmt.Errorf("paymentService/GetPaymentsList error: " + err.Error())
	}
	return payments, nil
}

func (s *service) GetPayment(ctx context.Context, payUUID string) (*types.Payment, error) {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("GetPayment")
		defer span.Finish()
	}
	pay, err := s.Storage.GetPay(ctx, payUUID)
	if err != nil {
		_ = level.Error(s.Logger).Log("paymentService/GetPayment error: ", err.Error())
		return nil, fmt.Errorf("paymentService/GetPayment error: " + err.Error())
	}
	return pay, nil
}
func (s *service) CreatePay(ctx context.Context, payRequest *types.PayRequest) error {
	if s.Tracing != nil {
		span := s.Tracing.StartSpan("CreatePay")
		defer span.Finish()
	}

	paymentID := uuid.New().String()
	errPay := s.Storage.CreatePay(ctx, paymentID, payRequest, false)
	if errPay != nil {
		errComment := s.Storage.UpdatePayComments(ctx, paymentID, errPay.Error())
		if errComment != nil {
			_ = level.Error(s.Logger).Log("paymentService/UpdatePayComments error: ", errComment.Error())
			return fmt.Errorf("paymentService/UpdatePayComments error: " + errComment.Error())
		}
		_ = level.Error(s.Logger).Log("paymentService/CreatePay error: ", errPay.Error())
		return fmt.Errorf("paymentService/CreatePay error: " + errPay.Error())
	}
	return nil
}
